/*
 * grunt-init-gruntplugin-sample
 * https://github.com/gruntjs/grunt-init-gruntplugin-sample
 *
 * Copyright (c) 2012 "Cowboy" Ben Alman
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function (grunt) {

    grunt.initConfig({
        watch: {
            options: {
                livereload: true,
            },
            js: {
                files: ['js/*.js', 'index.html']
            }
        },
        connect: {
            server: {
                options: {
                    base: '.',
                    //livereload: true
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');


    grunt.registerTask('w', ["connect", "watch"]);
}